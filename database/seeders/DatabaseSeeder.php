<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Role;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call(LaratrustSeeder::class);
          \App\Models\Personne::factory(50)->create();
        \App\Models\Region::factory(10)->create();
          \App\Models\Departement::factory(10)->create();
          $this -> call(UserSeeder::class);

    }
}
