<?php

namespace Database\Seeders;

use App\Http\Controllers\AdminController;
use App\Models\User;
use App\Models\Role;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $admin = new User();
        $admin->name = 'rince';
        $admin->email = 'administrateur@gmail.com';
        $admin->password = 'administrateur';
        $admin->save();
        $role = Role::where('name', 'administrateur')
        ->with('permissions')->first();
        $admin->addRole($role);
        foreach ($role->permissions as $permisson){
            $admin->givePermission($permisson);
        }

    }
}
