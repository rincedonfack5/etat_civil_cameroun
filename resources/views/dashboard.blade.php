@extends('layouts.app')
{{-- @section('content')
     <div class="row">
        <div class="col-12">
            <div class="page-title-container">
                <h1 class="mb-0 pb-0 display-4" id="title"></h1>
                <nav class="breadcrumb-container d-inline-block" aria-label="breadcrumb">
                    <ul class="breadcrumb pt-0">
                        <li class="breadcrumb-item"><a href="Dashboards.Default.html">Home</a></li>
                        <li class="breadcrumb-item"><a href="Dashboards.html">BIENVENUE A LA COMMUNE DE DSCHANG</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 col-lg-6">
            <div class="d-flex">
                <div class="dropdown-as-select me-3" data-setactive="false" data-childselector="span">
                    <a class="pe-0 pt-0 align-top lh-1 dropdown-toggle" href="#" data-bs-toggle="dropdown"
                        aria-expanded="false" aria-haspopup="true">
                        <span class="small-title"></span>
                    </a>
                    <div class="dropdown-menu font-standard">
                        <div class="nav flex-column" role="tablist">
                            <a class="active dropdown-item text-medium" href="#" aria-selected="true"
                                role="tab">Today's</a>
                            <a class="dropdown-item text-medium" href="#" aria-selected="false"
                                role="tab">Weekly</a>
                            <a class="dropdown-item text-medium" href="#" aria-selected="false"
                                role="tab">Monthly</a>
                            <a class="dropdown-item text-medium" href="#" aria-selected="false"
                                role="tab">Yearly</a>
                        </div>
                    </div>
                </div>
                <h2 class="small-title">Stats</h2>
            </div>

            <div class="mb-5">
                <div class="row g-2">
                    <div class="col-12 col-sm-6 col-lg-6">
                        <div class="card sh-11 hover-scale-up cursor-pointer">
                            <div class="h-100 row g-0 card-body align-items-center py-3">
                                <div class="col-auto pe-3">
                                    <div
                                        class="bg-gradient-light sh-5 sw-5 rounded-xl d-flex justify-content-center align-items-center">
                                        <i class="fa fa-child" aria-hidden="true"></i>                                    </div>
                                </div>
                                <div class="col">
                                    <div class="row gx-2 d-flex align-content-center">
                                        <div class="col-12 col-xl d-flex">
                                            <div class="d-flex align-items-center lh-1-25">ACTE DE NAISSSANCE</div>
                                        </div>
                                        <div class="col-12 col-xl-auto">
                                            <div class="cta-2 text-primary">{{ $naissance }}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-12 col-sm-6 col-lg-6">
                        <div class="card sh-11 hover-scale-up cursor-pointer">
                            <div class="h-100 row g-0 card-body align-items-center py-3">
                                <div class="col-auto pe-3">
                                    <div
                                        class="bg-gradient-light sh-5 sw-5 rounded-xl d-flex justify-content-center align-items-center">
                                        <i data-acorn-icon="check" class="text-white"></i>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="row gx-2 d-flex align-content-center">
                                        <div class="col-12 col-xl d-flex">
                                            <div class="d-flex align-items-center lh-1-25"> ACTE DE DECES</div>
                                        </div>
                                        <div class="col-12 col-xl-auto">
                                            <div class="cta-2 text-primary">{{ $deces }}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-lg-6">
                        <div class="card sh-11 hover-scale-up cursor-pointer">
                            <div class="h-100 row g-0 card-body align-items-center py-3">
                                <div class="col-auto pe-3">
                                    <div
                                        class="bg-gradient-light sh-5 sw-5 rounded-xl d-flex justify-content-center align-items-center">
                                        <i data-acorn-icon="alarm" class="text-white"></i>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="row gx-2 d-flex align-content-center">
                                        <div class="col-12 col-xl d-flex">
                                            <div class="d-flex align-items-center lh-1-25">DECLARATION DE NAISSANCE</div>
                                        </div>
                                        <div class="col-12 col-xl-auto">
                                            <div class="cta-2 text-primary">{{ $declaration}}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-lg-6">
                        <div class="card sh-11 hover-scale-up cursor-pointer">
                            <div class="h-100 row g-0 card-body align-items-center py-3">
                                <div class="col-auto pe-3">
                                    <div
                                        class="bg-gradient-light sh-5 sw-5 rounded-xl d-flex justify-content-center align-items-center">
                                        <i data-acorn-icon="sync-horizontal" class="text-white"></i>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="row gx-2 d-flex align-content-center">
                                        <div class="col-12 col-xl d-flex">
                                            <div class="d-flex align-items-center lh-1-25">DECLARATION DECES</div>
                                        </div>
                                        <div class="col-12 col-xl-auto">
                                            <div class="cta-2 text-primary"> {{$declarationdeces}}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-lg-6">
                        <div class="card sh-11 hover-scale-up cursor-pointer">
                            <div class="h-100 row g-0 card-body align-items-center py-3">
                                <div class="col-auto pe-3">
                                    <div
                                        class="bg-gradient-light sh-5 sw-5 rounded-xl d-flex justify-content-center align-items-center">
                                        <i data-acorn-icon="sync-horizontal" class="text-white"></i>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="row gx-2 d-flex align-content-center">
                                        <div class="col-12 col-xl d-flex">
                                            <div class="d-flex align-items-center lh-1-25"> COMMUNE</div>
                                        </div>
                                        <div class="col-12 col-xl-auto">
                                            <div class="cta-2 text-primary"> {{$commune }}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>

        </div>

        <div class="col-12 col-lg-6 mb-5">




            <div class="col-12 col-sm-6 col-lg-6">
                <div class="card sh-11 hover-scale-up cursor-pointer">
                    <div class="h-100 row g-0 card-body align-items-center py-3">
                        <div class="col-auto pe-3">
                            <div
                                class="bg-gradient-light sh-5 sw-5 rounded-xl d-flex justify-content-center align-items-center">
                                <i data-acorn-icon="sync-horizontal" class="text-white"></i>
                            </div>
                        </div>
                        <div class="col">
                            <div class="row gx-2 d-flex align-content-center">
                                <div class="col-12 col-xl d-flex">
                                    <div class="d-flex align-items-center lh-1-25">PERSONNEL</div>
                                </div>
                                <div class="col-12 col-xl-auto">
                                    <div class="cta-2 text-primary"> {{$employer}}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <br><br>



            <div class="col-12 col-sm-6 col-lg-6">
                <div class="card sh-11 hover-scale-up cursor-pointer">
                    <div class="h-100 row g-0 card-body align-items-center py-3">
                        <div class="col-auto pe-3">
                            <div
                                class="bg-gradient-light sh-5 sw-5 rounded-xl d-flex justify-content-center align-items-center">
                                <i data-acorn-icon="sync-horizontal" class="text-white"></i>
                            </div>
                        </div>
                        <div class="col">
                            <div class="row gx-2 d-flex align-content-center">
                                <div class="col-12 col-xl d-flex">
                                    <div class="d-flex align-items-center lh-1-25">REGION</div>
                                </div>
                                <div class="col-12 col-xl-auto">
                                    <div class="cta-2 text-primary">{{ $region}} </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>

            <div class="col-12 col-sm-6 col-lg-6">
                <div class="card sh-11 hover-scale-up cursor-pointer">
                    <div class="h-100 row g-0 card-body align-items-center py-3">
                        <div class="col-auto pe-3">
                            <div
                                class="bg-gradient-light sh-5 sw-5 rounded-xl d-flex justify-content-center align-items-center">
                                <i class="fa fa-user-md" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="col">
                            <div class="row gx-2 d-flex align-content-center">
                                <div class="col-12 col-xl d-flex">
                                    <div class="d-flex align-items-center lh-1-25">MEDECIN</div>
                                </div>
                                <div class="col-12 col-xl-auto">
                                    <div class="cta-2 text-primary">{{$medecin}} </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>



    </div>




@endsection --}}

@section('content')
    <strong>
        <h1 style="text-align: center">BIENVENUE A LA COMMUNE DE DSCHANG</h1>
    </strong>

    <div class="content-wrapper" style="margin-top: 30px">
        <div class="content">


            <!-- User Sessions Bounce -->
            <div class="row">
                <div class="col-xl-4">

                    <!-- User -->
                    <div class="card card-default">
                        <div class="card-header">
                            <h2>ACTE DE NAISSANCE</h2>
                        </div>
                        <div class="card-body">
                            <div
                                class="bg-primary d-flex justify-content-between flex-wrap p-5 text-white align-items-lg-end">
                                <div class="d-flex flex-column">
                                    <p class="h3 text-white">Nombre d'acte enregistrer</p>
                                    <h2 class="h3 text-white">{{ $naissance }}</h2>
                                    <span class="h3 text-white"></span>
                                </div>

                            </div>
                        </div>

                    </div>

                    <!-- User -->
                    <div class="card card-default">
                        <div class="card-header">
                            <h2>DECLARATION DECES</h2>
                        </div>
                        <div class="card-body">
                            <div
                                class="bg-info d-flex justify-content-between flex-wrap p-5 text-white align-items-lg-end">
                                <div class="d-flex flex-column">
                                    <span class="h3 text-white">Declaration decès</span>
                                    <span class="h2 text-white"> {{$declarationdeces}}</span>
                                </div>


                            </div>
                            <div id="line-chart-1"></div>

                        </div>
                    </div>

                </div>
                <div class="col-xl-4">

                    <!-- Session -->
                    <div class="card card-default">
                        <div class="card-header">
                            <h2>ACTE DE DECES</h2>
                        </div>
                        <div class="card-body">
                            <div
                                class="bg-primary d-flex justify-content-between flex-wrap p-5 text-white align-items-lg-end">
                                <div class="d-flex flex-column">
                                    <span class="h3 text-white">Nombre de decès enregistrer</span>
                                    <span class="h2 text-white">{{ $deces }}</span>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- Session -->
                    <div class="card card-default">
                        <div class="card-header">
                            <h2>EMPLOYER</h2>
                        </div>
                        <div class="card-body">
                            <div
                                class="bg-info d-flex justify-content-between flex-wrap p-5 text-white align-items-lg-end">
                                <div class="d-flex flex-column">
                                    <span class="h3 text-white">Nombre d'employer</span>
                                    <span class="h1 text-white">{{$employer}}</span>
                                </div>

                            </div>
                            <div id="line-chart-2"></div>

                        </div>
                    </div>

                </div>
                <div class="col-xl-4">

                    <!-- Bounce Rate -->
                    <div class="card card-default">
                        <div class="card-header">
                            <h2>DECLARATION NAISSANCE</h2>
                        </div>
                        <div class="card-body">
                            <div
                                class="bg-primary d-flex justify-content-between flex-wrap p-5 text-white align-items-lg-end">
                                <div class="d-flex flex-column">
                                    <span class="h3 text-white">Declaration de naissance</span>
                                    <span class="h1 text-white">{{ $declaration}}</span>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- Bounce Rate -->
                    <div class="card card-default">
                        <div class="card-header">
                            <h2>REGION</h2>
                        </div>
                        <div class="card-body">
                            <div
                                class="bg-info d-flex justify-content-between flex-wrap p-5 text-white align-items-lg-end">
                                <div class="d-flex flex-column">
                                    <span class="h3 text-white">LISTE DES REGIONS</span>
                                    <span class="h1 text-white" >{{ $region}}</span>
                                </div>

                            </div>
                        </div>
                        <div id="line-chart-3"></div>

                    </div>

                </div>
            </div>




        </div>

    </div>
@endsection
