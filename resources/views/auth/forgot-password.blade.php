{{-- <x-guest-layout>
    <div class="mb-4 text-sm text-gray-600">
        {{ __('Forgot your password? No problem. Just let us know your email address and we will email you a password reset link that will allow you to choose a new one.') }}
    </div>

    <!-- Session Status -->
    <x-auth-session-status class="mb-4" :status="session('status')" />

    <form method="POST" action="{{ route('password.email') }}">
        @csrf

        <!-- Email Address -->
        <div>
            <x-input-label for="email" :value="__('Email')" />
            <x-text-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required autofocus />
            <x-input-error :messages="$errors->get('email')" class="mt-2" />
        </div>

        <div class="flex items-center justify-end mt-4">
            <x-primary-button>
                {{ __('Email Password Reset Link') }}
            </x-primary-button>
        </div>
    </form>
</x-guest-layout> --}}



<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>login</title>
    <style>
        form {
            margin-top: 500px;
            width: 350px;
            height: 300px;
            margin-top: 80px;
            padding: 10px;
            background-color: rgba(255, 255, 255, 0.81);
        }

        input {
            width: 320px;
            padding: 8px;
            margin: 8px;
            border-radius: 7px;
            color: black;
            text-align: center;
        }

        body {
            background-image: url(vue-dessus-personnes-travaillant-ordinateurs-portables.jpg);
            background-size: cover;
        }

        button {
            width: 200px;
            height: 30px;
            background-color: green;
            color: white;
            border: none;
            margin-top: 20px;
        }

        a {
            margin-left: 10px;
            text-decoration: none;
            margin-top: 35px;
        }
        p{
            font-family: 'Times New Roman', Times, serif;
            font-size: 15px;

        }
    </style>

</head>

<body>
    <center>
        <form action="{{ route('password.email') }}" method="post">

            <img src="camer.png" alt="" style="width: 80px;">
            <p>Forgot your password? No problem. Just let us know your email address and we will email you a password
                reset link that will allow you to choose a new one.</p>

            <input type="email" placeholder="entrer votre adresse email" name="email">
            <button type="submit">Email Password Reset Link</button><br>

        </form>
    </center>
</body>

</html>
