<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Etat-civil</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <!-- Favicon -->
    <link href="img/favicon.ico" rel="icon">

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Heebo:wght@400;500;600&family=Nunito:wght@600;700;800&display=swap"
        rel="stylesheet">

    <!-- Icon Font Stylesheet -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">

    <!-- Libraries Stylesheet -->
    <link href="{{ asset('acceuil/lib/animate/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset('acceuil/lib/owlcarousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ asset('acceuil/lib/tempusdominus/css/tempusdominus-bootstrap-4.min.css" rel="stylesheet') }}" />

    <link href="{{ asset('acceuil/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('style.css') }}" rel="stylesheet">
</head>

<body>


    <!-- Topbar Start -->
    <div class="container-fluid bg-dark px-5 d-none d-lg-block">
        <div class="row gx-0">
            <div class="col-lg-8 text-center text-lg-start mb-2 mb-lg-0">
                <div class="d-inline-flex align-items-center" style="height: 45px;">
                    <small class="me-3 text-light"><i class="fa fa-map-marker-alt me-2"></i>Republique du cameroun
                    </small>
                    <small class="me-3 text-light"><i class="fa fa-phone-alt me-2"></i>+237 651673144</small>
                    <small class="text-light"><i class="fa fa-envelope-open me-2"></i>rincedonfack5@gmail.com</small>
                </div>
            </div>
            <div class="col-lg-4 text-center text-lg-end">
                <div class="d-inline-flex align-items-center" style="height: 45px;">
                    <a class="btn btn-sm btn-outline-light btn-sm-square rounded-circle me-2" href=""><i
                            class="fab fa-twitter fw-normal"></i></a>
                    <a class="btn btn-sm btn-outline-light btn-sm-square rounded-circle me-2" href=""><i
                            class="fab fa-facebook-f fw-normal"></i></a>
                    <a class="btn btn-sm btn-outline-light btn-sm-square rounded-circle me-2" href=""><i
                            class="fab fa-linkedin-in fw-normal"></i></a>
                    <a class="btn btn-sm btn-outline-light btn-sm-square rounded-circle me-2" href=""><i
                            class="fab fa-instagram fw-normal"></i></a>
                    <a class="btn btn-sm btn-outline-light btn-sm-square rounded-circle" href=""><i
                            class="fab fa-youtube fw-normal"></i></a>
                </div>
            </div>
        </div>
    </div>
    <!-- Topbar End -->


    <!-- Navbar & Hero Start -->
    <div class="container-fluid position-relative p-0">
        <nav class="navbar navbar-expand-lg navbar-light px-4 px-lg-5 py-3 py-lg-0">
            <a href="" class="navbar-brand p-0">
                <h1 class="text-primary m-0"><i class="fa fa-map-marker-alt me-3"></i>ETAT CIVIL</h1>
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
                <span class="fa fa-bars"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <div class="navbar-nav ms-auto py-0">
                    <a href="" class="nav-item nav-link active"></a>
                    <a href="" class="nav-item nav-link"></a>
                    <a href="" class="nav-item nav-link"></a>
                    <a href="package.html" class="nav-item nav-link"></a>

                    <a href="contact.html" class="nav-item nav-link">contact</a>
                </div>


                <a href="{{ route('login') }}" class="btn btn-primary rounded-pill py-2 px-4">connexion</a>


            </div>
        </nav>

        <div class="container-fluid bg-primary py-5 mb-5 hero-header">
            <div class="container py-5">
                <div class="row justify-content-center py-5">
                    <div class="col-lg-10 pt-lg-5 mt-lg-5 text-center">
                        <h1 class="display-3 text-white mb-3 animated slideInDown">BIENVENUE A L'ETAT-CIVIL DU CAMEROUN
                        </h1>
                        <p class="fs-4 text-white mb-4 animated slideInDown">Un acte d'Etat-civil est un document
                            officiel qui atteste d'un evenement important de la vie d'une personne tel que sa
                            naissance,son mariage ou son déces
                        </p>
                        <center>
                            <div class="position-relative w-75 mx-auto animated slideInDown">
                                <button type="button" style="height: 60px;"
                                    class="btn btn-primary  py-2 px-4 position-absolute top-0 end-0 me-2"
                                    style="margin-top: 7px;">Votre bien etre est notre prioriter chaque citoyen a droit
                                    au un acte de naissance </button>

                            </div>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Navbar & Hero End -->


    <!-- About Start -->
    <div class="container-xxl py-5">
        <div class="container">
            <div class="row g-5">
                <div class="col-lg-6 wow fadeInUp" data-wow-delay="0.1s" style="min-height: 400px;">
                    <div class="position-relative h-100">
                        <img class="img-fluid position-absolute w-100 h-100" src="acceuil/img/about.png"
                            alt="" style="object-fit: cover;">
                    </div>
                </div>
                <div class="col-lg-6 wow fadeInUp" data-wow-delay="0.3s">
                    <h6 class="section-title bg-white text-start text-primary pe-3">Cameroun</h6>
                    <h1 class="mb-4">Bienvenue A L'ETAT <span class="text-primary">CIVIL</span></h1>
                    <p class="mb-4">


                    </p>
                    <p class="mb-4">L’état civil au Cameroun est régi par l’ordonnance n° 81/002 du 29 juin 1981.
                        <br>
                        - Constatation juridique des événements : L’ordonnance régit la constatation juridique des
                        naissances, des mariages et des décès en République Unie du Cameroun. Elle fixe également les
                        conditions de validité des actes d’état civil
                        <br>
                        -Intangibilité des actes : Les actes de naissance, mariage et décès sont considérés comme
                        intangibles et définitifs. Ils ne peuvent être modifiés après signature que dans les conditions
                        prévues par la loi
                        <br>

                    </p>
                    <p style="font-family: Arial, Helvetica, sans-serif">
                        Déclaration des événements :
                        Tout Camerounais résidant au Cameroun est tenu de déclarer à l’officier d’état civil
                        territorialement compétent les naissances, les décès et les mariages le concernant, survenus ou
                        célébrés au Cameroun.
                        Les étrangers résidant au Cameroun doivent faire enregistrer ou transcrire sur les registres
                        d’état civil les événements survenus ou célébrés au Cameroun les concernant.
                        Dans les pays où le Cameroun dispose d’une mission diplomatique, les Camerounais doivent
                        déclarer ou faire transcrire les événements auprès du chef de mission diplomatique ou
                        consulaire. Les actes d’état civil établis en pays étrangers font foi s’ils ont été rédigés dans
                        les formes usitées dans ces pays
                    </p>



                </div>
            </div>
        </div>
    </div>
    <!-- About End -->


    <!-- Service Start -->
    <div class="container-xxl py-5">
        <div class="container">
            <div class="text-center wow fadeInUp" data-wow-delay="0.1s">
                <h6 class="section-title bg-white text-center text-primary px-3">Services</h6>
                <h1 class="mb-5">Les Services de l'Etat civil</h1>
            </div>
            <div class="row g-4">
                <div class="col-lg-3 col-sm-6 wow fadeInUp" data-wow-delay="0.1s">
                    <div class="service-item rounded pt-3">
                        <div class="p-4">
                            <i class="fa fa-3x fa-user text-primary mb-4"></i>

                            <h5>Acte de naissance</h5>
                            <p>
                                Un acte de naissance est un document juridique qui atteste de la naissance d'une
                                personnalité juridique.Il est dressé immédiatement par l'officier de l'état civil dès la
                                déclaration de naissance.
                                Pour obtenir un acte de naissance, il faut en faire la demande auprès de la mairie du
                                lieu où la personne concernée est née. Cette démarche peut être effectuée par
                                l’intéressé lui-même, ses parents, son conjoint, ses enfants, ou encore un professionnel
                                habilité (avocat, notaire). La demande peut être faite en ligne, par courrier ou
                                directement sur place.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 wow fadeInUp" data-wow-delay="0.3s">
                    <div class="service-item rounded pt-3">
                        <div class="p-4">
                            <i class="fa fa-3x fa-user text-primary mb-4"></i>

                            <h5>Etablissement des actes de mariage</h5>
                            <p>
                                Il constitue preuve juridique de la situation maritale des époux1. Un acte de mariage
                                est un document attestant du mariage de deux individus23. C'est un acte solennel par
                                lequel un homme et une femme ou deux personnes de même sexe établissent entre eux une
                                union dont les conditions les effets et la dissolution sont régis par les dispositions
                                juridiques en vigueur dans leur pays.

                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 wow fadeInUp" data-wow-delay="0.5s">
                    <div class="service-item rounded pt-3">
                        <div class="p-4">
                            <i class="fa fa-3x fa-user text-primary mb-4"></i>
                            <h5>Etablissement des actes de decès</h5>
                            <p>
                                L’acte de décès est un document administratif obligatoire et il est nécessaire pour
                                l’ensemble des démarches administratives.
                                Il contient toutes les informations d’état civil, liées au décès de la personne et à son
                                environnement familial, c’est-à-dire :

                                L’identité du défunt

                                La date, l’heure et le lieu de son décès

                                Sa date et le lieu de naissance

                                Son adresse et sa profession
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 wow fadeInUp" data-wow-delay="0.7s">
                    <div class="service-item rounded pt-3">
                        <div class="p-4">
                            <i class="fa fa-3x fa-cog text-primary mb-4"></i>
                            <h5>Declaration</h5>
                            <p>
                                Vérifié le 20 octobre 2023 - Direction de l'information légale et administrative
                                (Premier ministre), Ministère chargé de la justice

                                Vous venez d'avoir un enfant ? La déclaration de naissance est obligatoire pour tout
                                enfant né en France. Elle doit être faite par une personne ayant assisté à
                                l'accouchement. Elle permet d'établir l'acte de naissance. En cas de naissance d'un
                                enfant français à l'étranger, la déclaration doit être faite selon des formalités
                                spécifiques.
                            </p>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
    <!-- Service End -->




    <!-- Package Start -->
    <div class="container-xxl py-5">
        <div class="container">
            <div class="text-center wow fadeInUp" data-wow-delay="0.1s">
                <h6 class="section-title bg-white text-center text-primary px-3">Etat civil du Cameroun</h6>
                <h1 class="mb-5"> ALBUM COMMUNE DE DSCHANG</h1>
            </div>
            <div class="row g-4 justify-content-center">
                <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
                    <div class="package-item">
                        <div class="overflow-hidden">
                            <img class="img-fluid" src="acceuil/img/package-1.jpg" alt="">
                        </div>

                        <div class="text-center p-4">

                            <h4>Commune de dschang</h4>

                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.3s">
                    <div class="package-item">
                        <div class="overflow-hidden">
                            <img class="img-fluid" src="acceuil/img/package-2.jpg" alt="">
                        </div>

                        <div class="text-center p-4">
                            <h4>Cellule Informatique de Dchang</h4>
                            <p> Equipe de developpement</p>

                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.5s">
                    <div class="package-item">
                        <div class="overflow-hidden">
                            <img class="img-fluid" src="acceuil/img/package-3.jpg" alt="">
                        </div>

                        <div class="text-center p-4">

                            <h4>Chef d'equipe de developpement</h4>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- Package End -->



    <!-- Booking Start -->
    <div class="container-xxl py-5 wow fadeInUp" data-wow-delay="0.1s">
        <div class="container">
            <div class="booking p-5">
                <div class="row g-5 align-items-center">
                    <div class="col-md-6 text-white">
                        <h6 class="text-white text-uppercase">Etat civil</h6>
                        <h1 class="text-white mb-4">Etat civil cameroun</h1>
                        <p class="mb-4">Veuillez nous contacter en remplissant ce formulaire de contact
                        </p>

                    </div>
                    <div class="col-md-6">
                        <h1 class="text-white mb-4">Contacter </h1>
                        <form>
                            <div class="row g-3">
                                <div class="col-md-6">
                                    <div class="form-floating">
                                        <input type="text" class="form-control bg-transparent" id="name"
                                            placeholder="Your Name">
                                        <label for="name">Your Name</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-floating">
                                        <input type="email" class="form-control bg-transparent" id="email"
                                            placeholder="Your Email">
                                        <label for="email">Your Email</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-floating date" id="date3" data-target-input="nearest">
                                        <input type="text" class="form-control bg-transparent datetimepicker-input"
                                            id="datetime" placeholder="Date & Time" data-target="#date3"
                                            data-toggle="datetimepicker" />
                                        <label for="datetime">Date & Time</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-floating">
                                        <select class="form-select bg-transparent" id="select1">
                                            <option value="1">Destination 1</option>
                                            <option value="2">Destination 2</option>
                                            <option value="3">Destination 3</option>
                                        </select>
                                        <label for="select1">Destination</label>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-floating">
                                        <textarea class="form-control bg-transparent" placeholder="Special Request" id="message" style="height: 100px"></textarea>
                                        <label for="message">Special Request</label>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <button class="btn btn-outline-light w-100 py-3" type="submit">Book Now</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Booking Start -->





    <!-- Footer Start -->
    <div class="container-fluid bg-dark text-light footer pt-5 mt-5 wow fadeIn" data-wow-delay="0.1s">
        <div class="container py-5">
            <div class="row g-5">
                <div class="col-lg-3 col-md-6">
                    <h4 class="text-white mb-3">Company</h4>
                    <a class="btn btn-link" href="">About Us</a>
                    <a class="btn btn-link" href="">Contact Us</a>

                </div>
                <div class="col-lg-3 col-md-6">
                    <h4 class="text-white mb-3">Contact</h4>
                    <p class="mb-2"><i class="fa fa-map-marker-alt me-3"></i>Republique du cameroun</p>
                    <p class="mb-2"><i class="fa fa-phone-alt me-3"></i>+237 651673144</p>
                    <p class="mb-2"><i class="fa fa-envelope me-3"></i>rincedonfack5@example.com</p>
                    <div class="d-flex pt-2">
                        <a class="btn btn-outline-light btn-social" href=""><i class="fab fa-twitter"></i></a>
                        <a class="btn btn-outline-light btn-social" href=""><i
                                class="fab fa-facebook-f"></i></a>
                        <a class="btn btn-outline-light btn-social" href=""><i class="fab fa-youtube"></i></a>
                        <a class="btn btn-outline-light btn-social" href=""><i
                                class="fab fa-linkedin-in"></i></a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <h4 class="text-white mb-3">GallerI</h4>
                    <div class="row g-2 pt-2">
                        <div class="col-4">
                            <img class="img-fluid bg-light p-1" src="acceuil/img/package-1.jpg" alt="">
                        </div>
                        <div class="col-4">
                            <img class="img-fluid bg-light p-1" src="acceuil/img/package-2.jpg" alt="">
                        </div>
                        <div class="col-4">
                            <img class="img-fluid bg-light p-1" src="acceuil/img/package-3.jpg" alt="">
                        </div>
                        <div class="col-4">
                            <img class="img-fluid bg-light p-1" src="acceuil/img/package-2.jpg" alt="">
                        </div>
                        <div class="col-4">
                            <img class="img-fluid bg-light p-1" src="acceuil/img/package-3.jpg" alt="">
                        </div>
                        <div class="col-4">
                            <img class="img-fluid bg-light p-1" src="acceuil/img/package-1.jpg" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <h4 class="text-white mb-3">Newsletter</h4>
                    <p>Dolor amet sit justo amet elitr clita ipsum elitr est.</p>
                    <div class="position-relative mx-auto" style="max-width: 400px;">
                        <input class="form-control border-primary w-100 py-3 ps-4 pe-5" type="text"
                            placeholder="Your email">
                        <button type="button"
                            class="btn btn-primary py-2 position-absolute top-0 end-0 mt-2 me-2">SignUp</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="copyright">
                <div class="row">
                    <div class="col-md-6 text-center text-md-start mb-3 mb-md-0">

                        Designed By <a class="border-bottom">Donfack RINCE</a>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- Footer End -->


    <!-- Back to Top -->
    <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>


    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="{{ asset('acceuil/lib/wow/wow.min.js') }}"></script>
    <script src="{{ asset('acceuil/lib/easing/easing.min.js') }}"></script>
    <script src="{{ asset('acceuil/lib/waypoints/waypoints.min.js') }}"></script>
    <script src="{{ asset('acceuil/lib/owlcarousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('acceuil/lib/tempusdominus/js/moment.min.js') }}"></script>
    <script src="{{ asset('acceuil/lib/tempusdominus/js/moment-timezone.min.js') }}"></script>
    <script src="{{ asset('acceuil/lib/tempusdominus/js/tempusdominus-bootstrap-4.min.js') }}"></script>

    <!-- Template Javascript -->
    <script src="{{ asset('acceuil/js/main.js') }}"></script>
</body>

</html>
